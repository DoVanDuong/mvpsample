package gst.trainingcourse.gst_sample_mvp.utils

object Constant {
    const val EMPTY = ""
    const val INVALID_INPUT = "Please Input Information"
    const val SAME_PASSWORD = "Please enter 2 identical passwords"
    const val CHECK_REGISTER_TIME = 2000L
}