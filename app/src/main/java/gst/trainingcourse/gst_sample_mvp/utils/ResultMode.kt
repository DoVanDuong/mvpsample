package gst.trainingcourse.gst_sample_mvp.utils

enum class ResultMode() {
    EMPTY,
    ERROR,
    SUCCESS,
    SAME_PASSWORD
}