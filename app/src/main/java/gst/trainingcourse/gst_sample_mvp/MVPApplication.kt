package gst.trainingcourse.gst_sample_mvp

import android.app.Application

class MVPApplication : Application(){

    companion object {
        var INSTANCE: MVPApplication? = null
        fun getInstance(): MVPApplication = INSTANCE!!
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}