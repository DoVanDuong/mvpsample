package gst.trainingcourse.gst_sample_mvp.presenter

import gst.trainingcourse.gst_sample_mvp.model.Account

interface IRegisterPresenter {

    fun clickRegister(account: Account, passwordRepeat: String)

}