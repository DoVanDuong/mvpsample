package gst.trainingcourse.gst_sample_mvp.presenter

import gst.trainingcourse.gst_sample_mvp.model.Account
import gst.trainingcourse.gst_sample_mvp.repository.AccountRepository
import gst.trainingcourse.gst_sample_mvp.repository.usecase.CheckRegisterUseCase
import gst.trainingcourse.gst_sample_mvp.repository.usecase.CheckValidUseCase
import gst.trainingcourse.gst_sample_mvp.utils.ResultMode
import gst.trainingcourse.gst_sample_mvp.view.IMainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegisterPresenter(private var view: IMainActivity) : IRegisterPresenter {

    private val accountRepository = AccountRepository()

    private val checkValidUseCase = CheckValidUseCase(accountRepository)

    private val checkRegisterUseCase = CheckRegisterUseCase(accountRepository)

    override fun clickRegister(account: Account, passwordRepeat: String) {

        if (checkValidUseCase.checkValid(account)) {
            view.showLoginResult(ResultMode.EMPTY)
            return
        }

        if (checkValidUseCase.checkPassword(account.password, passwordRepeat)) {
            view.showLoginResult(ResultMode.SAME_PASSWORD)
            return

        }

        CoroutineScope(Dispatchers.IO).launch {
            if (checkRegisterUseCase.request(account)) {
                withContext(Dispatchers.Main) {
                    view.showLoginResult(ResultMode.SUCCESS)
                }
            } else {
                withContext(Dispatchers.Main) {
                    view.showLoginResult(ResultMode.ERROR)
                }
            }
        }

    }
}