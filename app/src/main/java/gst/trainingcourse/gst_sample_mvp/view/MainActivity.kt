package gst.trainingcourse.gst_sample_mvp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import gst.trainingcourse.gst_sample_mvp.R
import gst.trainingcourse.gst_sample_mvp.model.Account
import gst.trainingcourse.gst_sample_mvp.presenter.RegisterPresenter
import gst.trainingcourse.gst_sample_mvp.utils.Constant
import gst.trainingcourse.gst_sample_mvp.utils.ResultMode
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IMainActivity {

    private val registerPresenter: RegisterPresenter = RegisterPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAction()
    }

    private fun initAction() {

        btnRegister.setOnClickListener {

            val userName = edtUserName.text.toString()
            val password = edtPassword.text.toString()
            val passwordRepeat = edtPasswordRepeat.text.toString()
            val account = Account(userName, password)

            registerPresenter.clickRegister(account, passwordRepeat)
        }

    }

    override fun showLoginResult(result: ResultMode) {
        when (result) {
            ResultMode.EMPTY -> {
                Toast.makeText(this, Constant.INVALID_INPUT, Toast.LENGTH_LONG).show()
            }
            ResultMode.SAME_PASSWORD -> {
                Toast.makeText(this, Constant.SAME_PASSWORD, Toast.LENGTH_LONG).show()
            }
            ResultMode.ERROR -> {
                Toast.makeText(this, "Login Error", Toast.LENGTH_LONG).show()
            }
            ResultMode.SUCCESS -> {
                Toast.makeText(this, "Login Success", Toast.LENGTH_LONG).show()
            }
        }
    }
}