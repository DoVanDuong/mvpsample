package gst.trainingcourse.gst_sample_mvp.view

import gst.trainingcourse.gst_sample_mvp.utils.ResultMode

interface IMainActivity {
    fun showLoginResult(result: ResultMode)
}