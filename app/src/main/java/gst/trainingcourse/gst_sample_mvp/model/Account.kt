package gst.trainingcourse.gst_sample_mvp.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Account(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "user_name") var userName: String,
    @ColumnInfo(name = "password") var password: String
) {
    constructor(userName: String, password: String) : this(0, userName, password)
}