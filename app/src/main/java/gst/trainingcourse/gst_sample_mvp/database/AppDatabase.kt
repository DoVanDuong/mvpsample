package gst.trainingcourse.gst_sample_mvp.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import gst.trainingcourse.gst_sample_mvp.MVPApplication
import gst.trainingcourse.gst_sample_mvp.model.Account

@Database(entities = [Account::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(): AppDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: Room.databaseBuilder(
                MVPApplication.getInstance().applicationContext,
                AppDatabase::class.java,
                "My database"
            )
                .build()
                .also {
                    INSTANCE = it
                }
        }
    }

    abstract fun accountDAO(): AccountDAO
}