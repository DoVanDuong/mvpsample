package gst.trainingcourse.gst_sample_mvp.database

import android.database.Cursor
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import gst.trainingcourse.gst_sample_mvp.model.Account

@Dao
interface AccountDAO {
    @Insert
    fun insertAccount(vararg accounts: Account)

    @Query("SELECT * FROM account WHERE user_name = :userName")
    fun getAccount(userName: String): Cursor
}