package gst.trainingcourse.gst_sample_mvp.repository

import gst.trainingcourse.gst_sample_mvp.model.Account


interface IAccountRepository {

    fun checkPassword(password: String, passwordRepeat: String): Boolean

    fun checkRegister(account: Account): Boolean

    fun checkInvalidInput(account: Account): Boolean

}