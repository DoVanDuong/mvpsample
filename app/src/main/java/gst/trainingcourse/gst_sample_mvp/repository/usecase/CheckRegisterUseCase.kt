package gst.trainingcourse.gst_sample_mvp.repository.usecase

import gst.trainingcourse.gst_sample_mvp.model.Account
import gst.trainingcourse.gst_sample_mvp.repository.IAccountRepository

class CheckRegisterUseCase(private val accountRepository: IAccountRepository) {

    suspend fun request(account: Account): Boolean = accountRepository.checkRegister(account)

}