package gst.trainingcourse.gst_sample_mvp.repository.usecase

import gst.trainingcourse.gst_sample_mvp.model.Account
import gst.trainingcourse.gst_sample_mvp.repository.IAccountRepository

class CheckValidUseCase(private val accountRepository: IAccountRepository) {

    fun checkValid(account: Account): Boolean = accountRepository.checkInvalidInput(account)

    fun checkPassword(password: String, passwordRepeat: String): Boolean =
        accountRepository.checkPassword(password, passwordRepeat)

}