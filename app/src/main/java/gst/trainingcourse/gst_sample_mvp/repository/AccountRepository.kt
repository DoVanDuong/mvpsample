package gst.trainingcourse.gst_sample_mvp.repository

import gst.trainingcourse.gst_sample_mvp.database.AppDatabase
import gst.trainingcourse.gst_sample_mvp.model.Account

class AccountRepository : IAccountRepository {

    private var db: AppDatabase = AppDatabase.getInstance()

    override fun checkPassword(password: String, passwordRepeat: String): Boolean =
        password != passwordRepeat

    override fun checkRegister(account: Account): Boolean {

        val accountDAO = db.accountDAO()
        return try {
            val searchAccount = accountDAO.getAccount(account.userName)
            if (searchAccount.moveToFirst()) {
                false
            } else {
                accountDAO.insertAccount(account)
                true
            }
        } catch (e: Exception) {
            accountDAO.insertAccount(account)
            true
        }

    }

    override fun checkInvalidInput(account: Account): Boolean =
        (account.userName.isBlank() || account.password.isBlank())


}